<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'profile_photos';
    protected $fillable = ['path', 'name', 'thumbnail_path'];
    protected $file;


    public function  flyer()
    {

        return $this->belongsTo('App\Profile');
    }


    /**
     * base directory for saving photos uploaded
     * @return string
     */
    public function baseDir()
    {

        return 'images/photos';
    }

    /**
     * set the photo path and thumbnail path
     * @param $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->path = $this->baseDir() . '/' . $name;
        $this->thumbnail_path = $this->baseDir() . '/tn-' . $name;


    }

    /**
     * Delete a photo
     */
    public function delete(){
        \File::delete([

            $this->path,
            $this->thumbnail_path
        ]);

        parent::delete();
    }

}
