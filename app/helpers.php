<?php

use App\Http\Flash;

/**
 * autoload a flash instance to be available for all instances
 * @param null $title
 * @param null $message
 * @return \Illuminate\Foundation\Application|mixed
 */
function flash($title = null, $message = null)
{

    $flash = app(Flash::class);

    if (func_num_args() == 0) {
        return $flash;
    }
    return $flash->info($title, $message);
}

