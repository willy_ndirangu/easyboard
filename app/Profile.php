<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['name', 'email', 'description','contacts','address','user_id'];


    /**
     * @param $zip
     * @param $street
     * @return mixed
     */
    public static function locatedAt($email)
    {


        return static::where(compact('email'))->firstOrFail();
    }

    /*
     * add a photo to a profile and persist it
     */
    public function addPhoto(Photo $photo)
    {
        return $this->photos()->save($photo);
    }

    /**
     * A profile is composed of many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function photos()
    {

        return $this->hasMany('App\Photo');
    }

    public function owner()
    {

        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * check if a photo is owned by a particular user
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }
}
