<?php
namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;
Use App\Profile;
use App\Thumbnail;

class AddPhotoToProfile
{

    protected $profile;
    protected $file;

    public function __construct(Profile $profile, UploadedFile $file, Thumbnail $thumbnail=null)
    {
        $this->profile = $profile;
        $this->file = $file;
        $this->thumbnail= $thumbnail ?: new Thumbnail;
    }


    /**
     * save a photo associated with a particular user
     */
    public function save(){

        $photo=$this->profile->addPhoto($this->makePhoto());
        $this->file->move($photo->baseDir(),$photo->name);
        $this->thumbnail->make($photo->path,$photo->thumbnail_path);
    }

    /**
     * returns a new instance of the Photo
     * @return Photo
     */
    protected function makePhoto(){

        return new Photo(['name'=>$this->makeFileName()]);
    }

    /**
     * function to create a filename based on time of upload and the original name
     * @return string
     */
    protected function makeFileName()
    {
        $name= sha1(
            time() .$this->file->getClientOriginalname()
        );
        $extension =$this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }
}