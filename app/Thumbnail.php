<?php
namespace App;

use Intervention\Image\Facades\Image;

class Thumbnail
{
    /**
     *
     * @param $src -> source directory
     * @param $destination -> destination directory
     */
    public function make($src, $destination)
    {

        Image::make($src)
            ->fit(300)
            ->save($destination);

    }
}