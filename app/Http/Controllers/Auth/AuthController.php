<?php

namespace App\Http\Controllers\Auth;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    //redirected path
    protected $redirectPath = '/dashboard';
    //login path
    protected $loginPath = '/login';
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    /**
     * Function to authenticate users by checking if they are a board member or just a staff liaison
     * @param Request $request
     * @return redirect to desired page for staff liaison and board member
     */
    public function authenticate(Request $request)
    {


        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...


            //check if the user is a board member and if it's the first
            // time they are logging in then prompt them
            // to change the password
            if (Auth::user()->first_login === true && Auth::user()->board_member === true) {
                flash()->overlay("Welcome", " Enter Your Email Address to obtain a password reset link ");
                (User::where('email', '=', $request->email)->update(array('first_login' => false)));
                Auth::logout();
                return redirect('password/email');


            } else if (Auth::user()->first_login === false && Auth::user()->board_member === true) {
                flash()->overlay("A board Member", "Welcome");
                return redirect('dashboard');
            } else {
                flash()->overlay(" Staff Liaison", "Welcome to the Admin dashboard");
                return redirect('dashboard');

            }

        } else {

            flash()->error("Login Error", " Password and email do not match ");
            return redirect()->back();
        }
    }



    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'first_login' => false,
            'board_member' => false,
        ]);
    }
}
