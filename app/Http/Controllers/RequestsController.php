<?php

namespace App\Http\Controllers;

use App\Event;
use App\Profile;
use App\User;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class RequestsController extends Controller
{
    /*
    * Protect the routes
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $request = \App\Request::get()->first();
        $event = Event::findOrFail($request->event_id)->first();
        $profile = Profile::findOrFail($request->profile_id)->first();


        return view('pages.board_requests.show', compact(['request', 'event', 'profile']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($user_id, $event_id)
    {
        $profile = Profile::where('user_id', '=', $user_id)->first();
        $event = Event::findOrFail($event_id)->first();

        \App\Request::create([
            'profile_id' => $profile->id,
            'event_id' => $event->id,
            'description' => $profile->name . " has requested for transport and accommodation",

        ]);
        flash()->overlay("Transport and Accommodation Request", "getting that sorted out ASAP");
        return redirect()->route('show_events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $eventid)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
