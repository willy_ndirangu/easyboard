<?php

namespace App\Http\Controllers;

use App\Update;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatesRequest;

class PagesController extends Controller
{
    /*
   * Protect the routes
   */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function dashboard()
    {

        return view('pages.dashboard');
    }


}
