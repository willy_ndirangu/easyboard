<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use App\User;
use App\Profile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfilesController extends Controller
{
    /*
   * Protect the routes
   */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        //add a record in the users table
        $this->createBoardUser($request);
        //Create a board member profile
        $this->createProfile($request);



        return $this->redirect_path($request->email);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        $profile = Profile::locatedAt($email);

        return view('pages.profiles.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::where('id', $id)->first();

        return view('pages.profiles.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Profile::where('id', '=', $id)->update(array('description' => $request->description,
            'name' => $request->name,
            'address' => $request->address,
            'contacts' => $request->contacts));
        flash()->overlay("Update Made", "Redirecting to profiles ");
        return redirect('show/profiles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return back();
    }


    /**
     * Create a board member profile
     * @param $data variable containing the user profile data
     */
    public function createProfile($data)
    {

        if (Profile::where('email', '=', $data->email)->exists()) {
            flash()->error("User already in system", "try another member");
            return redirect()->back();
        } else {
            $id=User::where('email', '=', $data->email)->first();

            Profile::create([
                'name' => $data->name,
                'email' => $data->email,
                'user_id'=>$id->id,
                'address' => $data->address,
                'description' => $data->description,
                'contacts' => $data->contact,

            ]);
            flash()->overlay("Board Member " . $data->name . " ", "has been Added to the system");
        }

    }

    /**
     * create a board user associated with a particular profile
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createBoardUser($data)
    {
        if (User::where('email', '=', $data->email)->exists()) {
            flash()->error("User already in system", "try another member");
            return redirect('/password/email');
        } else {
            User::create([
                'name' => $data->name,
                'email' => $data->email,
                'password' => bcrypt($data['password']),
                'first_login' => true,
                'board_member' => true,
            ]);
        }


    }

    /**
     * Redirect to the show profiles view with data associated with the particular profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function redirect_path($attribute)
    {

        return redirect()->route('profile', $attribute);
    }

    /**
     * show all profiles
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfiles()
    {
        $profiles = Profile::orderBy('created_at', 'desc')->get();

        return view('pages.profiles.profiles', compact('profiles'));
    }


}
