<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use App\AddPhotoToProfile;
use App\Photo;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PhotosController extends Controller
{
    /*
   * Protect the routes
   */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * function to store a photo associated with a particular profile
     * @param $email
     * @param Request $request
     */
    public function store($email, Request $request)
    {
        $profile = Profile::locatedAt($email);
        $photo = $request->file("photo");
        (new AddPhotoToProfile($profile, $photo))->save();


    }


    /**
     * Delete a photo associated with a particular profile
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Photo::findOrFail($id)->delete();

        return back();
    }
}
