<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//staff views

Route::get('dashboard', 'PagesController@dashboard')->name('dashboard');
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('login_authenticate');
Route::get('login', 'Auth\AuthController@getLogin')->name('login');
Route::post('login', 'Auth\AuthController@authenticate')->name('login');
Route::get('logout', 'Auth\AuthController@getLogout')->name('logout');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('reset');
Route::post('password/reset', 'Auth\PasswordController@postReset')->name('reset');

// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister')->name('register');
Route::post('register', 'Auth\AuthController@postRegister')->name('register');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('email_password_reset_link');
Route::post('password/email', 'Auth\PasswordController@postEmail')->name('email_password_reset_link');

//
Route::get('board/create','ProfilesController@create')->name('board');
Route::post('board/create','ProfilesController@store')->name('board');
Route::get('show/profile/{email}','ProfilesController@show')->name('profile');
Route::get('show/profiles','ProfilesController@showProfiles')->name('profiles');
Route::get('update/profile/{id}','ProfilesController@edit')->name('update_profiles');
Route::post('update/profile/{id}','ProfilesController@update')->name('update_profiles');
Route::get('delete/profile/{id}','ProfilesController@destroy')->name('delete_profile');

//Photos
Route:post('{email}/photos','PhotosController@store');
Route::delete('photos/{id}','PhotosController@destroy');

//updates
Route::get('updates/create','UpdatesController@index')->name('create_updates');
Route::post('updates/create','UpdatesController@store')->name('create_updates');
Route::get('updates/show','UpdatesController@show')->name('show_updates');
Route::get('update/event/{id}', "UpdatesController@edit")->name('update_event');
Route::post('update/event/{id}', "UpdatesController@update")->name('update_event');

//events
Route::get('create/event', "EventsController@index")->name('create_event');
Route::post('create/event', "EventsController@store")->name('create_event');
Route::get('events/show', "EventsController@show")->name('show_events');
Route::get('edit/event/{id}', "EventsController@edit")->name('edit_event');
Route::post('edit/event/{id}', "EventsController@update")->name('edit_event');

//Add Minutes
Route::get('minute','MinutesController@index')->name('create_minutes');
Route::post('create/minutes','MinutesController@store')->name('create_minutes');
Route::get('view/minutes','MinutesController@create')->name('display_minutes');

//requests
Route::get('request/{id}/{events}','RequestsController@store');
Route::get('show/requests','RequestsController@create');