var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss'),
        mix.copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js/jquery.min.js'),
        mix.copy('bower_components/tinymce/', 'public/js/tinymce'),
        mix.copy('node_modules/tinymce/tinymce.js', 'resources/assets/js/tinymce.js'),
        mix.copy('node_modules/foundation-sites/dist/foundation.min.js', 'resources/assets/js/foundation.min.js'),
        mix.copy('bower_components/angular/angular.min.js', 'resources/assets/js/angular.min.js'),
        mix.copy('bower_components/angular-ui-router/release/angular-ui-router.min.js', 'resources/assets/js/angular-ui-router.min.js'),
        mix.copy('bower_components/dropzone/dist/min/dropzone.min.js', 'resources/assets/js/dropzone.min.js'),
        mix.copy('bower_components/dropzone/dist/min/dropzone.min.css', 'resources/assets/css/dropzone.min.css'),
        mix.copy('bower_components/sweetalert/dist/sweetalert-dev.js', 'resources/assets/js/sweetalert.dev.js'),
        mix.copy('bower_components/tinymce/tinymce.js', 'resources/assets/js/tinymce.min.js'),
        mix.copy('bower_components/sweetalert/dist/sweetalert.css', 'resources/assets/css/sweetalert.css'),
        mix.copy('bower_components/foundation-icon-fonts/foundation-icons.css', 'resources/assets/css/foundation-icons.css'),
        mix.copy('bower_components/foundation-icon-fonts/foundation-icons.ttf', 'public/css/foundation-icons.ttf'),
        mix.copy('bower_components/foundation-icon-fonts/foundation-icons.woff', 'public/css/foundation-icons.woff'),
        mix.scripts([
            "jquery.min.js",
            "foundation.min.js",
            "angular.min.js",
            "angular-ui-router.min.js",
            "clientRouting.js",
            "dropzone.min.js",
            "sweetalert.dev.js",
            "tinymce.js",


            "nav.js"

        ]),
        mix.styles([
            "dropzone.min.css",
            "foundation-icons.css",
            "sweetalert.css"
        ]);
});
