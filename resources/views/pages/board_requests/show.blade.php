@extends('pages.dashboard')

@section('content')


    <div class="row">

        <div class="medium-6 columns">
            <div class="box">
                <div class="box-icon">
                    <span class="fi-calendar"></span>
                </div>
                <div class="info">

                        <h4 class="text-center">{{$profile->name}}</h4>


                        <h4 class="text-center">{{$event->title}}</h4>
                        <h5 class="text-center">Location {{$event->location}}</h5>
                        <h6 class="text-center"> Starting at {{$event->start_date}} {{$event->start_time}}</h6>
                        <h6 class="text-center"> Ending at {{$event->end_date}} {{$event->end_time}}</h6>



                        <p>{{$request->description}}</p>



                </div>
            </div>
        </div>
    </div>




@stop