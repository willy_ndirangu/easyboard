@extends('pages.dashboard')

@section('content')


    <div class="row">
        @foreach( $updates as $update)
        <div class="medium-6 columns">
            <div class="box">
                <div class="box-icon">
                    <span class="fi-info"></span>
                </div>
                <div class="info">
                    <h4 class="text-center">{{$update->title}}</h4>
                    <h6 class="text-center"> created at {{$update->created_at}}</h6>
                    <p>{{$update->description}}</p>
                    <a href="/update/event/{{$update->id}}" class="button">Link</a>
                </div>
            </div>
        </div>
        @endforeach
    <div>


@stop