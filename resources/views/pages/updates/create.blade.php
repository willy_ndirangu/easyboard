
@extends('pages.dashboard')

@section('content')
    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>




            <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'create_updates', 'class' => 'form')) !!}

            {!! csrf_field() !!}
            <div class="row column  ">

                <h4 class="text-center">Schedule an update for board members </h4>

                {!! Form::label('Title') !!}
                {!! Form::text('title', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Board Member Full name *')) !!}




                {!! Form::label('Description') !!}
                {!! Form::textArea('description', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'A brief description of the update')) !!}





                {!! Form::submit('Schedule an Update',
                  array('class'=>'button expanded ')) !!}


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
