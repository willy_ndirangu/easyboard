@extends('pages.dashboard')


@section('content')

    <div class="row">
        <div class="medium-6 columns">

            {!! Form::model($event,['url'=>'/update/event/' .$event->id]) !!}

            {!! Form::label('Edit the Title') !!}
            {!! Form::text('title', null, [ ]) !!}
            {!! Form::label('Edit the description') !!}
            {!! Form::textArea('description', null, [ ]) !!}


            {!! Form::submit('edit Description!',
              array('class'=>'button')) !!}



            {!! Form::close() !!}

        </div>
    </div>

@stop