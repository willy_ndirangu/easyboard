
@extends('pages.dashboard')

@section('content')
    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>




            <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'create_event', 'class' => 'form')) !!}

            {!! csrf_field() !!}
            <div class="row column  ">

                <h4 class="text-center">Schedule an update for board members </h4>

                {!! Form::label('Title of the event') !!}
                {!! Form::text('title', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Events Title')) !!}

                {!! Form::label('location') !!}
                {!! Form::text('location', null,
                  array('required',
                        'class'=>'form-control',
                        'placeholder'=>'Event location')) !!}

                {!! Form::label('start date ') !!}
                {!! Form::date('start_date', null,
                    array('required')) !!}
                {!! Form::label('start  time') !!}
                {!! Form::time('start_time', null,
                    array('required')) !!}

                {!! Form::label('End date') !!}
                {!! Form::date('end_date', null,
                    array('required')) !!}
                {!! Form::label('End time') !!}
                {!! Form::time('end_time', null,
                    array('required')) !!}

                {!! Form::label('Description') !!}
                {!! Form::textArea('description', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'A brief description of the update')) !!}





                {!! Form::submit('create Event',
                  array('class'=>'button expanded ')) !!}


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
