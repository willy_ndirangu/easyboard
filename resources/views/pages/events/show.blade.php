@extends('pages.dashboard')

@section('content')


    <div class="row">
        @foreach( $events as $event)
            <div class="medium-6 columns">
                <div class="box">
                    <div class="box-icon">
                        <span class="fi-calendar"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-center">{{$event->title}}</h4>
                        <h5 class="text-center">Location {{$event->location}}</h5>
                        <h6 class="text-center"> Starting at {{$event->start_date}} {{$event->start_time}}</h6>
                        <h6 class="text-center"> Ending at {{$event->end_date}} {{$event->end_time}}</h6>

                        <p>{{$event->description}}</p>
                        @if(!Auth::user()->board_member)
                            <a href="/edit/event/{{$event->id}}" class="button">View Details</a>
                            @else
                            <a href="/request/{{Auth::user()->id}}/{{$event->id}}" class="button">Request for Transport and Accommodation</a>
                        @endif

                    </div>
                </div>
            </div>
    </div>
        @endforeach



@stop