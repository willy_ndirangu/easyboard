@extends('pages.dashboard')


@section('content')

    <div class="row">
        <div class="medium-6 columns">

            {!! Form::model($event,['url'=>'/edit/event/' .$event->id]) !!}

            {!! Form::label('Edit the events title') !!}
            {!! Form::text('title', null, [ ]) !!}
            {!! Form::label('Edit the Location') !!}
            {!! Form::text('location', null, [ ]) !!}
            {!! Form::label('Edit the start date and  time') !!}
            {!! Form::date('start_date', null, [ ]) !!}
            {!! Form::time('start_time', null, [ ]) !!}
            {!! Form::label('Edit the start date and  time') !!}
            {!! Form::date('end_date', null, [ ]) !!}
            {!! Form::time('end_time', null, [ ]) !!}
            {!! Form::label('Edit the Events description') !!}
            {!! Form::textArea('description', null, [ ]) !!}


            {!! Form::submit('edit event!',
              array('class'=>'button')) !!}



            {!! Form::close() !!}

        </div>
    </div>

@stop