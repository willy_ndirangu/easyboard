
@extends('pages.dashboard')

@section('content')
    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>




            <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'board', 'class' => 'form')) !!}

            {!! csrf_field() !!}
            <div class="row column  ">

                <h4 class="text-center">Create a board member profile on  easyboard</h4>

                {!! Form::label('Full Name') !!}
                {!! Form::text('name', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Board Member Full name *')) !!}




                {!! Form::label(' E-mail Address') !!}
                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Board Member address *')) !!}

                {!! Form::label('Contacts') !!}
                {!! Form::number('contact', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Board Member telephone contacts *')) !!}

                {!! Form::label(' Postal Address') !!}
                {!! Form::text('address', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'xxxxx-xxxxx')) !!}



                {!! Form::label('Assign first login Password') !!}
                {!! Form::password('password', null,
                    array('required',
                          'class'=>'show-for-sr')) !!}

                {!! Form::label('Confirm Password') !!}
                {!! Form::password('password_confirmation', null,
                    array('required',
                          'class'=>'show-for-sr')) !!}

                {!! Form::label('Description') !!}
                {!! Form::textArea('description', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Board Member Description')) !!}




                {!! Form::submit('Create a profile',
                  array('class'=>'button expanded ')) !!}


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
