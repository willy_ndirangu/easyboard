@extends('pages.dashboard')


@section('content')

    <div class="row">
        <div class="medium-6 columns">

            {!! Form::model($profile,['url'=>'/update/profile/' .$profile->id]) !!}

            {!! Form::label('Edit the Name') !!}
            {!! Form::text('name', null, [ ]) !!}
            {!! Form::label('Edit the Address') !!}
            {!! Form::text('address', null, [ ]) !!}
            {!! Form::label('Edit the contacts') !!}
            {!! Form::text('contacts', null, [ ]) !!}
            {!! Form::label('Edit the description') !!}
            {!! Form::textArea('description', null, [ ]) !!}


            {!! Form::submit('edit Member!',
              array('class'=>'button')) !!}



            {!! Form::close() !!}

        </div>
    </div>

@stop