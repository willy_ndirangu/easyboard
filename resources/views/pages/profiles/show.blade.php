@extends('pages.dashboard')

@section('content')
    <div class="row">
        <div class="medium-6 columns ">

            <h3> Name: {!! $profile->name !!}</h3>

            <h3>Email Address: {!! $profile->email !!}</h3>
            <hr>
            <p>{!! nl2br($profile->description) !!}</p>
            <a href="/update/profile/{{$profile->id}}" class="button">Change User Details</a>
        </div>


        <div class="small-4 medium-6 columns  ">
            <h3> Contact: {!! $profile->contacts !!}</h3>

            <h3> Address: {!! $profile->address !!}</h3>
            <hr>


            <div class="row">
                @foreach ($profile->photos->chunk(4) as $set)

                        @foreach ($set as $photo)

                        <div class="small-4 medium-4 columns end">
                            <form method="POST" action="/photos/{{$photo->id}}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE" id="">
                                <button type="submit">Delete</button>
                            </form>
                            <a href="/{{$photo->path}}" data-lity>
                                <img src="/{{ $photo->thumbnail_path }}" alt="" style="width:100%">
                            </a>


                    </div>
                    @endforeach
                @endforeach
            </div>


            <form id="addPhotosForm"
                  action="/{{$profile->email}}/photos"
                  method="POST"
                  class="dropzone">

                {{csrf_field()}}
            </form>
        </div>
    </div>

@stop

@section('scripts.footer')

    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFilesize: 5,
            acceptedFiles: '.jpg, .jpeg, .png,.bmp'
        };
    </script>

@stop