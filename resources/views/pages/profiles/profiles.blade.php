@extends('pages.dashboard')

@section('content')


    <div class="row">
        @foreach($profiles as $profile)
            <div class="medium-6 columns">
                <div class="box">
                    <div class="box-icon">
                        <span class="fi-torso-business"></span>
                    </div>
                    <div class="info">
                        <h4 class="text-left">{{$profile->name}}</h4>
                        <h6 class="text-left"> Contacts {{$profile->contacts}}</h6>
                        <h6 class="text-left"> Address {{$profile->address}}</h6>
                        @foreach ($profile->photos->chunk(4) as $set)

                            @foreach ($set as $photo)

                                <div class="small-4 medium-4 columns end">
                                <a href="/{{$photo->path}}" data-lity>
                                    <img src="/{{ $photo->thumbnail_path }}" alt="">
                                </a>

                                    </div>

                            @endforeach

                        @endforeach

                        <p>{{$profile->description}}</p>

                        @if(!Auth::user()->board_member)
                            <a href="/show/profile/{{$profile->email}}" class="button">Profile Details</a>
                            <a href="/delete/profile/{{$profile->user_id}}" class="button">delete Profile</a>
                        @endif
                    </div>
                </div>
            </div>

        @endforeach
    </div>
@stop

