<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/all.css">


</head>
<body>

@if(Auth::check())

    @if(!Auth::user()->board_member)
        @include('partials._admin-dashboard')

    @else
        @include('partials._member-dashboard')
    @endif

@endif


@yield('content')






<script src="/js/all.js"></script>

@include('pages.flash')
@yield('scripts.footer')
</body>
</html>




