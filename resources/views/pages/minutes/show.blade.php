@extends('pages.dashboard')

@section('content')

    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="medium-6 medium-centered large-6 large-centered columns">

            @foreach($minutes as $minute)
               {!!$minute->content  !!}
                <br>
        </div>

    </div>
    @endforeach
@stop


