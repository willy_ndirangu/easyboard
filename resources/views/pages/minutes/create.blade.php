


@extends('pages.dashboard')

@section('content')


    <div class="row">
        <div class="medium-11 medium-centered large-11 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>




            <!---errors--->
            <!---login form---->
            <form action="/create/minutes" method="post" autocomplete="on">
                <h4 class="text-center">Add Minutes Here </h4>
                <textarea name="content" id="minutes" cols="30" rows="10">
                    text
                </textarea>



            {!! csrf_field() !!}

                <input type="submit" class="button ">
        </form>

        </div>
    </div>


@stop

@section('scripts.footer')
    <script type="text/javascript">
        tinyMCE.baseURL = "/js/tinymce";
        tinymce.init({
            selector: '#minutes',
            content_css: "../css/all.css",
            theme:'modern'

        });


    </script>

@stop


