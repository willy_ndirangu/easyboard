<!--
 * Cytonn Technologies
 * @author Ndirangu Wilson <wndirangu@gmail.com>
--->

<!doctype html>
<html ng-app="dashboard"  lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Board Management Application</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/all.css">

</head>
<body>

<div class="row">
    <div class="medium-6 medium-centered ">
        <h1><img src="/images/pages/logo.png" alt="some_text">&nbsp; Cytonn &nbsp; Easyboard</h1>
    </div>
    </hr>

</div>


        <!-- Authentication content loading -->
@yield('content')


<script src="/js/all.js"></script>

@include('pages.flash')
</body>
</html>