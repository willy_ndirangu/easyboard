<!-- Small Navigation -->
<div class="title-bar" data-responsive-toggle="nav-menu" data-hide-for="medium">
    <a class="logo-small show-for-small-only" href="/dashboard"><img src="/images/pages/dashboard.png"/></a>
    <button class="menu-icon" type="button" data-toggle></button>
    <div class="title-bar-title">Menu</div>
</div>

<!-- Medium-Up Navigation -->
<nav class="top-bar " id="nav-menu">

    <div class="logo-wrapper hide-for-small-only">
        <div class="logo">
            <img src="/images/pages/dashboard.png">
        </div>
    </div>

    <!-- Left Nav Section -->
    <div class="top-bar-left">
        <ul class="vertical medium-horizontal menu">
            <li><a href="/board/create">Create board member profile</a></li>
            <li><a href="/show/profiles">View profiles</a></li>
            <li><a href="/show/requests">Board member requests</a></li>


        </ul>
    </div>

    <!-- Right Nav Section -->
    <div class="top-bar-right">

        <ul class="vertical medium-horizontal dropdown menu" data-dropdown-menu>
            <li class="has-submenu">
                <a>Minutes</a>
                <ul class="submenu menu vertical" data-submenu>
                    <li><a href="/minute">Add Minutes</a></li>
                    <li><a href="/view/minutes">View Minutes</a></li>
                </ul>

            </li>
            <li class="has-submenu">
                <a>Updates</a>
                <ul class="submenu menu vertical" data-submenu>
                    <li><a href="/updates/create">Schedule an Update</a></li>
                    <li><a href="/updates/show">View Updates</a></li>
                </ul>

            </li>


            <li class="has-submenu">
                <a>Events</a>
                <ul class="submenu menu vertical" data-submenu>
                    <li><a href="/create/event/">create an event</a></li>
                    <li><a href="/events/show">View events</a></li>
                </ul>
            </li>
            <li>
                <a href="/logout"> Logout {!! Auth::user()->name !!}</a>
            </li>
        </ul>
    </div>

</nav>