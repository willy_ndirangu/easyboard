@extends('auth')

@section('content')

    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @if(session('status'))

                {!! flash()->overlay("Reset","Check your email for the password reset link") !!}
                @endif

                        <!---errors--->
                <!---login form---->
                {!! Form::open(array('route' => 'email_password_reset_link', 'class' => 'form')) !!}

                {!! csrf_field() !!}
                <div class="row column log-in-form ">

                    <h4 class="text-center"> Reset Password</h4>


                    {!! Form::label('Your E-mail Address') !!}
                    {!! Form::text('email', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Your e-mail address *')) !!}




                    {!! Form::submit('Send the link',
                      array('class'=>'button expanded ')) !!}


                </div>
                {!! Form::close() !!}

        </div>
    </div>
@stop
