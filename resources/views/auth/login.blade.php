@extends('auth')

@section('content')

    @if(Auth::check())
        {!! Auth::logout() !!}
    @endif

    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>


            <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'login', 'class' => 'form')) !!}
            {!! csrf_field() !!}
            <div class="row column log-in-form ">

                <h4 class="text-center">Log in with you email account</h4>


                {!! Form::label('Your E-mail Address') !!}
                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your e-mail address *')) !!}


                {!! Form::label('Password') !!}
                {!! Form::password('password', null,
                    array('required',
                          'class'=>'show-for-sr')) !!}





                {!! Form::submit('login',
                  array('class'=>'button expanded ')) !!}


                <p class="text-center"><a href="password/email">Forgot your password?</a></p>


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
