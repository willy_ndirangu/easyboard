@extends('auth')

@section('content')

    <div class="row">
        <div class="medium-6 medium-centered large-6 large-centered columns">
            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>




                    <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'register', 'class' => 'form')) !!}

            {!! csrf_field() !!}
            <div class="row column log-in-form ">

                <h4 class="text-center">Register for an account with easyboard</h4>

                {!! Form::label('Full Name') !!}
                {!! Form::text('name', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your full name *')) !!}




                {!! Form::label('Your E-mail Address') !!}
                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your e-mail address *')) !!}


                {!! Form::label('Password') !!}
                {!! Form::password('password', null,
                    array('required',
                          'class'=>'show-for-sr')) !!}

                {!! Form::label('Confirm Password') !!}
                {!! Form::password('password_confirmation', null,
                    array('required',
                          'class'=>'show-for-sr')) !!}



                {!! Form::submit('Register',
                  array('class'=>'button expanded ')) !!}


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
