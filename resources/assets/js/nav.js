$(document).foundation();
$(".off-canvas-submenu").hide();
$(".off-canvas-submenu-call").click(function() {
    var icon = $(this).parent().next(".off-canvas-submenu").is(':visible') ? '+' : '-';
    $(this).parent().next(".off-canvas-submenu").slideToggle('fast');
    $(this).find("span").text(icon);
});
Dropzone.options.addPhotosForm = {
    paramName: 'photo',
    maxFilesize: 5,
    acceptedFiles: '.jpg, .jpeg, .png,.bmp'
};


