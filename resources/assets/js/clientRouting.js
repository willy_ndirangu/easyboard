var dashboard = angular.module('dashboard', ["ui.router"]);
dashboard.config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to world
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('createProfile', {
            url: "/board/create",
            templateUrl: "/views/pages/profiles/create.blade.php"
        })




});
